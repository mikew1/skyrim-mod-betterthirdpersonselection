#pragma once
#include "Offsets.h"

// Special Edition Offsets
#if not defined(SKYRIMAE)

// float* Offsets::DeltaTime = (float*)REL::ID(523660).address();                       // 2F6B948
// float* Offsets::DeltaTimeRealTime = (float*)REL::ID(523661).address();               // 2F6B94C
// float* Offsets::DurationOfApplicationRunTimeMS = (float*)REL::ID(523662).address();  // 2F6B950
// 
// float* Offsets::HUDOpacity = (float*)REL::ID(510579).address();  // 1DF58F8

uintptr_t          Offsets::WorldToCamMatrix = REL::ID(519579).address();              // 2F4C910
RE::NiRect<float>* Offsets::ViewPort = (RE::NiRect<float>*)REL::ID(519618).address();  // 2F4DED0

// Raycast (from SmoothCam)
uintptr_t Offsets::CameraCaster = REL::ID(32270).address();
uintptr_t Offsets::GetNiAVObject = REL::ID(76160).address();

float Offsets::GetWorldScale()
{
	REL::Relocation<float*> worldScale{ REL::ID(231896) };
	return *worldScale;
}

float Offsets::GetWorldScaleInverse()
{
	REL::Relocation<float*> worldScaleInverse{ REL::ID(230692) };
	return *worldScaleInverse;
}

REL::Relocation<Offsets::DismountActor_func> Offsets::DismountActor{ REL::ID(36882) };  // Actor::Dismount: 605f70

// Anniversary Edition offsets
#else

uintptr_t Offsets::WorldToCamMatrix = REL::ID(406126).address(); //AE
RE::NiRect<float>* Offsets::ViewPort = (RE::NiRect<float>*)REL::ID(406160).address(); //AE

// Raycast (from SmoothCam)
uintptr_t Offsets::CameraCaster = REL::ID(33007).address(); //AE
uintptr_t Offsets::GetNiAVObject = REL::ID(77988).address(); //AE

float Offsets::GetWorldScale()
{
	REL::Relocation<float*> worldScale{ REL::ID(188105) };  // AE - 141637AA0
	return *worldScale;
}

float Offsets::GetWorldScaleInverse()
{
	REL::Relocation<float*> worldScaleInverse{ REL::ID(187407) };  // AE - 14162DF48
	return *worldScaleInverse;
}

REL::Relocation<Offsets::DismountActor_func> Offsets::DismountActor{ REL::ID(37906) };  // AE - 62E390 -
#endif
