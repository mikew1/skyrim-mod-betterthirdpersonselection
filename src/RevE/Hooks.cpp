#pragma once

#include "Hooks.h"
#include "NearbyActorScanner.h"
#include "FocusManager.h"
#include "Settings.h"
#include "API/Compatibility.h"
#include "Offsets.h"
#include "Input/InputHandler.h"
#include "Filters/FilterManager.h"

#include <UI/SelectionWidget.h>
#include "UI/DebugAPI.h"

#include <xbyak/xbyak.h>

namespace Hooks
{
	void Install()
	{
		logger::info("BTPS applying hooks...");

		ActivateHook::Hook();
		SelectionHook::Hook();
		MainUpdateHook::Hook();
		HorseBackHook::Hook();
		UpdateCrosshairTextHook::Hook();

		logger::info("BTPS finished applying hooks...");
	}

	void MainUpdateHook::Hook()
	{
		logger::info("BTPS applying MainUpdateHook");

		auto& trampoline = SKSE::GetTrampoline();

		#if not defined(SKYRIMAE)
			REL::Relocation<uintptr_t> hook{ REL::ID(35551) };  // main loop
			_Update = trampoline.write_call<5>(hook.address() + 0x11F, Update);
		#else
			REL::Relocation<uintptr_t> hook{ REL::ID(36544) };  // AE - 5D29F0, main loop
			_Update = trampoline.write_call<5>(hook.address() + 0x160, Update);
		#endif
	}

	void MainUpdateHook::Update(RE::Main* a_this, float a2)
	{
		_Update(a_this, a2);

		Settings::OnUpdateSettingsState();
	}

	RE::TESObjectREFR* SelectionHook::OnSetCrosshairTargetRef(RE::ObjectRefHandle objectRefHandle)
	{
		auto crosshairPick = RE::CrosshairPickData::GetSingleton();
		if (!crosshairPick)
			return nullptr;

		RE::TESObjectREFR* objectRef = nullptr;
		if (objectRefHandle && objectRefHandle.get())
			objectRef = objectRefHandle.get().get();

		if (FocusManager::IsEnabled)
		{
			auto focusObj = FocusManager::GetCustomFocusObject(objectRef);
			FocusManager::SetFocusObject(focusObj);

			if (focusObj.get())
                return focusObj->ObjectRef.get().get();
			return nullptr;
		}
		else
		{
			if (Settings::EnableFiltersForNativeSelection && FilterManager::ShouldFilterObj(objectRef))
				objectRef = nullptr;

			// filter out native object selection for some objects on horseback - eg. furniture
			// because activating furniture from horseback breaks things
			if (objectRef && !NearbyActorScanner::HasBlockingFlagsHorseback(*objectRef) )
			{
				FocusManager::SetFocusObject(objectRef);
				return objectRef;
			}
			else
			{
				FocusManager::SetFocusObject(nullptr);
				return nullptr;
			}
		}
	}

	void SelectionHook::Hook()
	{
		logger::info("BTPS applying SelectionHook");

		struct Patch : Xbyak::CodeGenerator
		{
			Patch(REL::Relocation<std::uintptr_t> jmpBackAddress)
			{
				// move objectRef into register
				//mov(ptr[r15 + 4], eax);

				mov(rcx, eax);

				// call OnSetCrosshairTargetRef
				mov(rax, (size_t)&OnSetCrosshairTargetRef);
				call(rax);

				// move return value into rbx
				mov(rbx, rax);

				// mov from original asm that is overridden as well
				mov(rcx, rbx);

				// jump back
				mov(r10, jmpBackAddress.address());
				jmp(r10);
			}
		};

#if not defined(SKYRIMAE)
		// 3AA4B0 - CrossHairPickData::Pick
		// 0xCF0  - mov in the function where the new focus is applied
		REL::Relocation<std::uintptr_t> target{ REL::ID(25591), 0xCF0 };
		REL::Relocation<std::uintptr_t> jmpBackAddress{ REL::ID(25591), 0xCF7 }; // this is right after the changed assembly
#else
		// 3C1DD8
		// 3C0FA0 - CrossHairPickData::Pick
		// 0xDA8  - mov in the function where the new focus is applied
		REL::Relocation<std::uintptr_t> target{ REL::ID(26127), 0xE38 }; //AE
		REL::Relocation<std::uintptr_t> jmpBackAddress{ REL::ID(26127), 0xE3F };  // AE this is right after the changed assembly
#endif

		Patch code(jmpBackAddress);
		code.ready();

		auto& trampoline = SKSE::GetTrampoline();

		trampoline.write_branch<6>(target.address(),
			trampoline.allocate(code)
			);
	}

	// form types that can be activated using native Skyrim object picker
	bool SelectionHook::IsValidFormType(RE::FormType t)
	{
		return
			t == RE::FormType::Activator ||
			t == RE::FormType::ActorCharacter ||
			t == RE::FormType::NPC ||
			t == RE::FormType::Door ||
			t == RE::FormType::Furniture ||
			t == RE::FormType::TalkingActivator ||
			t == RE::FormType::Apparatus;
	}

	void ActivateHook::Actor_Dismount(RE::Actor* a_this)
	{
		// if hold to dismount is disabled, allow native dismount behaviour
		if (!Settings::IsHoldToDismountEnabled)
			Offsets::DismountActor(a_this);
	}

	void ActivateHook::Hook()
	{
		logger::info("BTPS applying ActivateHook");

		auto& trampoline = SKSE::GetTrampoline();

 		REL::Relocation<std::uintptr_t> ActivateHandlerVtbl{ RE::Offset::ActivateHandler::Vtbl };
 		_ProcessButton = ActivateHandlerVtbl.write_vfunc(0x4, ProcessButton);

#ifdef SKYRIMAE
		REL::Relocation<std::uintptr_t> dismountHookTarget{ REL::ID(37864), 0xE2 };  //AE - untested
#else
		// 601a00 / 36840 - function where the dismount for activation key happens
		// 0xE2 - offset to the call to Actor::Dismount
		REL::Relocation<std::uintptr_t> dismountHookTarget{ REL::ID(36840), 0xE2 };
#endif

		trampoline.write_call<5>(dismountHookTarget.address(), Actor_Dismount);
	}

	void ActivateHook::ProcessButton(RE::ActivateHandler* a_this, RE::ButtonEvent* a_event, RE::PlayerControlsData* a_data)
	{
		auto playerChar = RE::PlayerCharacter::GetSingleton();

		// if player is on mount, override activation mechanics
        if (Settings::IsHoldToDismountEnabled && playerChar && playerChar->IsOnMount() && 
			a_event->heldDownSecs >= Settings::HoldToDismountMinTime)
 		{
			auto menu = RE::UI::GetSingleton()->GetMenu(SelectionWidgetMenu::MENU_NAME);

			if (a_event->IsHeld())
			{
                if (a_event->heldDownSecs - Settings::HoldToDismountMinTime >= Settings::HoldToDismountTime)
				{
					Offsets::DismountActor(playerChar);

					SelectionWidget::SetProgressCirlceValue(0.0);

					a_this->heldButtonActionSuccess = true;

					return;
				}
				else
				{
                    auto value = (a_event->heldDownSecs - Settings::HoldToDismountMinTime) / Settings::HoldToDismountTime;
					SelectionWidget::SetProgressCirlceValue((float)value);
				}
			}
			else if(a_event->IsUp())
			{
				SelectionWidget::SetProgressCirlceValue(0.0);
			}
		}
		else
			SelectionWidget::SetProgressCirlceValue(0.0);

		_ProcessButton(a_this, a_event, a_data);
	}

	void UpdateCrosshairTextHook::Hook()
	{
		auto& trampoline = SKSE::GetTrampoline();

#ifdef SKYRIMAE
		REL::Relocation<std::uintptr_t> target{ REL::ID(40621), 0x280 }; // AE - untested
		_OnUpdateCrosshairText = trampoline.write_call<5>(target.address(), OnUpdateCrosshairText);

		//REL::Relocation<std::uintptr_t> target_autoLoadDoor{ REL::ID(51622), 0x22B }; // AE - untested
		//_OnUpdateCrosshairText_AutoLoadDoor = trampoline.write_call<5>(target.address(), OnUpdateCrosshairText_AutoLoadDoor);
#else
		REL::Relocation<std::uintptr_t> target{ REL::ID(39535), 0x289 };
		_OnUpdateCrosshairText = trampoline.write_call<5>(target.address(), OnUpdateCrosshairText);

		//REL::Relocation<std::uintptr_t> target_autoLoadDoor{ REL::ID(50727), 0xD7 };
		//_OnUpdateCrosshairText_AutoLoadDoor = trampoline.write_call<5>(target.address(), OnUpdateCrosshairText_AutoLoadDoor);
#endif
	}

	void UpdateCrosshairTextHook::OnUpdateCrosshairText(RE::UIMessageQueue* a_this, const RE::BSFixedString& a_menuName, RE::UI_MESSAGE_TYPE a_type, RE::IUIMessageData* a_data)
	{
		_OnUpdateCrosshairText(a_this, a_menuName, a_type, a_data);

		if (auto data = a_data ? static_cast<RE::HUDData*>(a_data) : nullptr; data)
		{
			SelectionWidget::OnChangeActivateText();
		}
	}

	void UpdateCrosshairTextHook::OnUpdateCrosshairText_AutoLoadDoor(RE::UIMessageQueue* a_this, const RE::BSFixedString& a_menuName, RE::UI_MESSAGE_TYPE a_type, RE::IUIMessageData* a_data)
	{
// 		_OnUpdateCrosshairText(a_this, a_menuName, a_type, a_data);
// 
// 		if (auto data = a_data ? static_cast<RE::HUDData*>(a_data) : nullptr; data)
// 		{
// 			SelectionWidget::OnChangeActivateText_AutoLoadDoor();
// 		}
	}

	void HorseBackHook::Hook()
	{
		logger::info("BTPS applying HorseBackHook");

		struct Patch_IsOnMount : Xbyak::CodeGenerator
		{
			Patch_IsOnMount(REL::Relocation<std::uintptr_t> jmpBackAddress)
			{
				// jump back
				mov(r10, jmpBackAddress.address());
				jmp(r10);
			}
		};

		auto& trampoline = SKSE::GetTrampoline();

#ifdef SKYRIMAE
		// 0x6d9980
		// 0xAA	- call Actor__IsOnMount
		REL::Relocation<std::uintptr_t> target{ REL::ID(40621), 0xAA }; // AE
		REL::Relocation<std::uintptr_t> jmpBackAddress{ REL::ID(40621), 0xB7 }; //AE

		REL::Relocation<std::uintptr_t> HUD_Fix{ REL::ID(51612), 0x12BC };  // AE - func: 8add90 - offset: 8AF04C
#else
		// 0x6b0570		- PlayerCharacter::sub_1406B0570
		// 0xAA			- call    Actor__IsOnMount_14022F320
		REL::Relocation<std::uintptr_t> target{ REL::ID(39535), 0xAA };
		REL::Relocation<std::uintptr_t> jmpBackAddress{ REL::ID(39535), 0xB7 }; // this is right after the changed assembly

		REL::Relocation<std::uintptr_t> HUD_Fix{ REL::ID(50718), 0x1209 }; // 87d580
#endif

		Patch_IsOnMount code(jmpBackAddress);
		code.ready();

		trampoline.write_branch<6>(target.address(),
			trampoline.allocate(code)
			);

		// override a call to IsOnMount with a function that always returns false
		trampoline.write_call<5>(HUD_Fix.address(), FalseFunc);
	}

}
