class BTPS_API
{
public:
	
	static BTPS_API* GetSingleton();

	virtual bool SelectionEnabled();
    virtual bool Widget3DEnabled();

	virtual void HideSelectionWidget(std::string source);
    virtual void ShowSelectionWidget(std::string source);
};
