#pragma once
#include "SelectionWidget.h"
#include "RevE/Offsets.h"
#include "RevE/Hooks.h"
#include "FocusManager.h"
#include "API/Compatibility.h"
#include "Settings.h"
#include "UI/DebugAPI.h"

#include "windows.h"

// cached values to avoid querying values from flash all the time

glm::vec2 SelectionWidget::WidgetPos;
glm::vec2 SelectionWidget::WidgetSize;

//

bool SelectionWidget::OriginalValuesSet = false;

bool SelectionWidget::IsUsingTelekinesis = false;

std::string SelectionWidget::LastActivateText = "";
std::string SelectionWidget::LastActivateName = "";

void SelectionWidget::OnChangeActivateText()
{
	UpdateWidgetText();

	SelectionWidgetMenu::Show("SetNewFocusObject");
}

glm::vec2 SelectionWidget::OrigMCPos;
glm::vec2 SelectionWidget::OrigMCSize;

double SelectionWidget::Widget2DSize = 1.0;
glm::vec2 SelectionWidget::Widget2DPos = glm::vec2(0.5, 0.35);

float SelectionWidget::ProgressCircleValue = 0.0f;

std::vector<std::string> SelectionWidgetMenu::Hidden_Sources;
bool SelectionWidgetMenu::IsEnabled = false;
bool SelectionWidgetMenu::IsVisible = false;

void SelectionWidgetMenu::SetEnabled(bool mode)
{
	if (IsEnabled != mode)
	{
		IsEnabled = mode;
		ToggleVisibility(true);
	}
}

double SelectionWidget::GetMaxCameraDist()
{
	auto playerChar = RE::PlayerCharacter::GetSingleton();

	if (!playerChar || !playerChar->IsOnMount())
		return MAX_CAMERA_DIST;
	return MAX_CAMERA_DIST_HORSEBACK;
}

void SelectionWidget::Update()
{
	HideNativeHUD();
	UpdateTDMVisibility();
	UpdateWidgetPos();
}

void SelectionWidget::UpdateWidgetPos()
{
	auto playerChar = RE::PlayerCharacter::GetSingleton();
	if (!playerChar)
		return;

	auto ui = RE::UI::GetSingleton();
	if (!ui)
		return;

	auto widgetMenu = ui->GetMenu(SelectionWidgetMenu::MENU_NAME);
	if (!widgetMenu || !widgetMenu->uiMovie)
		return;

	InitHUDValues();
	if (!OriginalValuesSet)
		return;

	RE::GFxValue selectionWidget;
	widgetMenu->uiMovie->GetVariable(&selectionWidget, "mc_SelectionWidget");
	if (!selectionWidget.IsObject())
		return;

	selectionWidget.SetMember("_alpha", Settings::WidgetAlpha);

	UpdateProgressCirlce(widgetMenu);
    CheckNeedsTextUpdate();

	auto focusObj = FocusManager::ActiveFocusObject;

	glm::dvec3 widgetPos;
	if (!Settings::Widget3DEnabled ||
		!Get3DWidgetPos(focusObj.get(), widgetPos)) // Get3DWidgetPos fails on things like AutoLoadDoors for example - draw those 2D
	{
        WidgetPos = Get2DWidgetPos(widgetMenu);
		WidgetSize = Get2DWidgetSize();

		SetElementPos(WidgetPos, selectionWidget);
		SetElementSize(WidgetSize, selectionWidget);

		if (Compatibility::MoreHUD::IsEnabled)
			UpdateMoreHUDWidget();
		return;
	}

	WidgetPos = GetScreenLoc(widgetMenu, widgetPos);
	WidgetPos = ClampWidgetToScreenRect(widgetMenu, WidgetPos, selectionWidget);

	WidgetSize = Get3DWidgetSize(focusObj.get());

 	SetElementSize(WidgetSize, selectionWidget);
 	SetElementPos(WidgetPos, selectionWidget);

	if (Compatibility::MoreHUD::IsEnabled)
		UpdateMoreHUDWidget();
}

void SelectionWidget::CheckNeedsTextUpdate()
{
	auto ui = RE::UI::GetSingleton();
	if (!ui) return;

	auto hud = ui->GetMenu(RE::HUDMenu::MENU_NAME);
	if (!hud || !hud->uiMovie) return;

	RE::GFxValue rolloverText;
	
	hud->uiMovie->GetVariable(&rolloverText, "HUDMovieBaseInstance.RolloverText");

	auto nativeHtmlString = GetGFxMember(rolloverText, "htmlText");
	auto stringName = GetGFxMember(rolloverText, "text");

	auto focusObj = FocusManager::ActiveFocusObject;
	bool quickLootActive = Compatibility::QuickLootRE::IsEnabled && focusObj && focusObj->ObjectRef.get() && Compatibility::QuickLootRE::CanOpen(focusObj->ObjectRef.get().get());
	
	std::string currActivateText;
	std::string currActivateName;

	try
	{
		currActivateText = std::string(nativeHtmlString.ToString());
		currActivateName = std::string(stringName.ToString());
	}
	// possible fix for crash: '(char*) "string too long"'
	catch (std::exception e)
	{
		currActivateText = " - ";
		currActivateName = " - ";
	}

	if (currActivateName != LastActivateName)
	{
		// QuickLootRE empties the activate text again explicitly when switching containers. I'm attempting to undo this here
		// without having to hook into the QuickLoot binaries
        if (Settings::Widget3DEnabled && quickLootActive && Util::IsEmpty(currActivateName) && !Util::IsEmpty(LastActivateName))
		{
            currActivateText = LastActivateText;
            rolloverText.SetTextHTML(currActivateText.c_str());
		}

		LastActivateText = currActivateText;
        LastActivateName = currActivateName;
		OnChangeActivateText();
	}
}

constexpr float ACTIVATE_BUTTON_OFFSET_LEFT = 10.0;

void SelectionWidget::UpdateWidgetText()
{
	auto ui = RE::UI::GetSingleton();
	if (!ui) return;

	auto hud = ui->GetMenu(RE::HUDMenu::MENU_NAME);
	if (!hud || !hud->uiMovie) return;

	auto widgetMenu = ui->GetMenu(SelectionWidgetMenu::MENU_NAME);
	if (!widgetMenu || !widgetMenu->uiMovie) return;

	RE::GFxValue selectionWidget;
	widgetMenu->uiMovie->GetVariable(&selectionWidget, "mc_SelectionWidget");
	if (!selectionWidget.IsObject()) return;

	RE::GFxValue rolloverText;
	RE::GFxValue rolloverInfoText;
	RE::GFxValue rolloverGrayBar_mc;	
	RE::GFxValue rolloverButton_tf;

	hud->uiMovie->GetVariable(&rolloverText, "HUDMovieBaseInstance.RolloverText");
	hud->uiMovie->GetVariable(&rolloverInfoText, "HUDMovieBaseInstance.RolloverInfoText");
	hud->uiMovie->GetVariable(&rolloverButton_tf, "HUDMovieBaseInstance.RolloverButton_tf");
	hud->uiMovie->GetVariable(&rolloverGrayBar_mc, "HUDMovieBaseInstance.RolloverGrayBar_mc");

	// ClearUI compatibility - probably messes with the actionscript somewhere and removes 'RolloverGrayBar_mc'
	if (!rolloverGrayBar_mc.IsObject())
		hud->uiMovie->GetVariable(&rolloverGrayBar_mc, "HUDMovieBaseInstance.RolloverInfo_mc.RolloverInfoInstance");

	if (!rolloverText.IsObject() || !rolloverInfoText.IsObject() || !rolloverGrayBar_mc.IsObject() || !rolloverButton_tf.IsObject())
		return;

	auto nativeHtmlString = GetGFxMember(rolloverText, "htmlText");
	auto stringName = GetGFxMember(rolloverText, "text");
    auto htmlStringInfo = GetGFxMember(rolloverInfoText, "htmlText");
	auto stringInfo = GetGFxMember(rolloverInfoText, "text");
	auto htmlStringButton = GetGFxMember(rolloverButton_tf, "htmlText");

	auto activateText = GetGFxMember(selectionWidget, "ActivateText");
	auto infoText = GetGFxMember(selectionWidget, "InfoText");
	auto grayBar = GetGFxMember(selectionWidget, "GrayBar");

	std::string sanitizedHTMLString = 
		Compatibility::MoreHUD::IsEnabled ? 
		Compatibility::MoreHUD::StripHTMLIconPlaceholders(nativeHtmlString.ToString().c_str()) : 
		nativeHtmlString.ToString().c_str();

	if (!activateText.IsObject() || !infoText.IsObject() || !grayBar.IsObject())
		return;

	auto activateButton = GetGFxMember(selectionWidget, "ActivateButtonInstance");
	if (!activateButton.IsObject())
		return;

	auto activateButtonText = GetGFxMember(activateButton, "Text");
	if (!activateButtonText.IsObject())
		return;

	auto focusObj = FocusManager::ActiveFocusObject;

	// disable activate button for containers if quickloot is installed
    bool quickLootActive = Compatibility::QuickLootRE::IsEnabled && focusObj && focusObj->ObjectRef.get() && Compatibility::QuickLootRE::CanOpen(focusObj->ObjectRef.get().get());

    activateText.SetTextHTML(sanitizedHTMLString.c_str());
	infoText.SetTextHTML(htmlStringInfo.GetString());

	activateButtonText.SetTextHTML(htmlStringButton.GetString());

	bool grayBarVisible = Settings::IsDividerEnabled && !Util::IsEmpty(stringInfo.GetString());

	// show if infotext isn't empty
	grayBar.SetMember("_visible", grayBarVisible);
	infoText.SetMember("_visible", Settings::IsItemInfoEnabled);

	// if button is disabled globally, or no activation text, or the first line of the activation text is empty
	if (IsUsingTelekinesis || // no activate button for telekinesis target
        quickLootActive || 
		!Settings::IsActivationButtonEnabled || 
		(Util::IsEmpty(stringName.GetString()) && !grayBarVisible) || 
		Util::IsFirstLineEmpty(stringName.GetString()))
	{
		activateButton.SetMember("_visible", false);
		return;
	}

	RE::GFxValue lineMetricsOut;
	RE::GFxValue args[1]{ 0 };
	activateText.Invoke("getLineMetrics", &lineMetricsOut, args, 1);

 	auto lineMetricsX = GetGFxMember(lineMetricsOut, "width").GetNumber();
	lineMetricsX = PixelToStageCoordinates(widgetMenu, glm::vec2(lineMetricsX, 0.0)).x;

	activateButton.SetMember("_x", -lineMetricsX - ACTIVATE_BUTTON_OFFSET_LEFT);
	activateButton.SetMember("_visible", true);
}

void SelectionWidget::HideNativeHUD()
{
	auto ui = RE::UI::GetSingleton();
	if (!ui)
		return;

	auto hud = ui->GetMenu(RE::HUDMenu::MENU_NAME);
	if (!hud || !hud->uiMovie)
		return;

	RE::GFxValue rolloverText;
	RE::GFxValue rolloverInfoText;
	RE::GFxValue rolloverGrayBar_mc;
	RE::GFxValue rolloverButton_tf;

	hud->uiMovie->GetVariable(&rolloverText, "HUDMovieBaseInstance.RolloverText");
	hud->uiMovie->GetVariable(&rolloverInfoText, "HUDMovieBaseInstance.RolloverInfoText");
	hud->uiMovie->GetVariable(&rolloverButton_tf, "HUDMovieBaseInstance.RolloverButton_tf");
	hud->uiMovie->GetVariable(&rolloverGrayBar_mc, "HUDMovieBaseInstance.RolloverGrayBar_mc");

	// ClearUI compatibility - probably messes with the actionscript somewhere and removes 'RolloverGrayBar_mc'
	if (!rolloverGrayBar_mc.IsObject())
		hud->uiMovie->GetVariable(&rolloverGrayBar_mc, "HUDMovieBaseInstance.RolloverInfo_mc.RolloverInfoInstance");

	if (!rolloverText.IsObject() || !rolloverInfoText.IsObject() || !rolloverGrayBar_mc.IsObject() || !rolloverButton_tf.IsObject())
		return;

	rolloverText.SetMember("_visible", false);
	rolloverInfoText.SetMember("_visible", false);
	rolloverGrayBar_mc.SetMember("_visible", false);
	rolloverButton_tf.SetMember("_visible", false);
}

void SelectionWidget::UpdateProgressCirlce(RE::GPtr<RE::IMenu> menu)
{
	if (!menu || !menu->uiMovie)
		return;

	RE::GFxValue progressCirlce;
	menu->uiMovie->GetVariable(&progressCirlce, "ProgressCircleInstance");

	if (!progressCirlce.IsObject())
		return;

	progressCirlce.SetMember("_alpha", Settings::WidgetAlpha);

	if (ProgressCircleValue == 0.0f)
		progressCirlce.SetMember("_visible", false);
	else
	{
		progressCirlce.SetMember("_visible", true);

		RE::GFxValue args[1]{ ProgressCircleValue };
		progressCirlce.Invoke("SetProgressCircleProgress", nullptr, args, 1);
	}
}

void SelectionWidget::UpdateTDMVisibility()
{
	using tdmCompat = Compatibility::TrueDirectionalMovement;
    auto focusObj = FocusManager::ActiveFocusObject;

	if (tdmCompat::IsEnabled && tdmCompat::IsTrueHUDEnabled && Settings::Widget3DEnabled &&
		focusObj && focusObj->ObjectRef.get())
	{
        auto actorRef = focusObj->ObjectRef.get()->As<RE::Actor>();

        if (tdmCompat::TrueHUDAPIInterface)
        {
            if (actorRef)
            {
                auto actorRefHandle = actorRef->GetHandle();
                auto hasInfoBar = tdmCompat::TrueHUDAPIInterface->HasInfoBar(actorRefHandle);

				if (hasInfoBar)
				{
                    SelectionWidgetMenu::Hide("TDM");
                    return;
				}
            }
        }
		// less good fallback solution for old versions of TrueHUD - registering the API
		// will have failed in that case, despite TrueHUD being installed
        else
		{
            if (actorRef && actorRef->IsInCombat() &&
                actorRef->GetLifeState() == RE::ACTOR_LIFE_STATE::kAlive)
			{
				SelectionWidgetMenu::Hide("TDM");
				return;
			}
		}
	}

	SelectionWidgetMenu::Show("TDM");
}

glm::vec2 SelectionWidget::ToGlobal(glm::vec2 posIn, RE::GFxValue element, RE::GPtr<RE::IMenu> menu)
{
	if (!menu || !menu->uiMovie || !element.IsObject())
		return posIn;

	RE::GFxValue objPoint;
	menu->uiMovie->CreateObject(&objPoint);
	objPoint.SetMember("x", posIn.x);
	objPoint.SetMember("y", posIn.y);

	RE::GFxValue gfxResult;
	RE::GFxValue args[1]{ objPoint };
	element.Invoke("globalToLocal", &gfxResult, args, 1);

	double newX = GetGFxMember(args[0], "x").GetNumber();
	double newY = GetGFxMember(args[0], "y").GetNumber();

	return glm::vec2(newX, newY);

}

double MoreHUDIconSpace = 10.0;
double MoreHUDEffectsSizeFact = 1.25;
double InfoWidgetOffsetLeft = 50.0;

double testValueX = 640.0;
double testValueY = -25.0;

void SelectionWidget::UpdateMoreHUDWidget()
{
	auto ui = RE::UI::GetSingleton();
	if (!ui) return;

	auto hud = ui->GetMenu(RE::HUDMenu::MENU_NAME);
	if (!hud || !hud->uiMovie) return;

	auto widgetMenu = ui->GetMenu(SelectionWidgetMenu::MENU_NAME);
	if (!widgetMenu || !widgetMenu->uiMovie) return;

	RE::GFxValue iconCont;
	hud->uiMovie->GetVariable(&iconCont, "ahz.scripts.widgets.AHZHudInfoWidget.IconContainer");
	if (!iconCont.IsObject()) return;

	auto textField = GetGFxMember(iconCont, "_tf");
	if (!textField.IsObject()) return;
	
	auto hudRoot = GetGFxMember(textField, "_parent");
	if (!hudRoot.IsObject()) return;

	auto loadedIcons = GetGFxMember(iconCont, "loadedIcons");
	if (!loadedIcons.IsArray()) return;

	auto iconSize = GetGFxMember(iconCont, "_iconSize");
	if (!iconSize.IsNumber()) return;

	auto iconScale = GetGFxMember(iconCont, "_iconScale");
	if (!iconScale.IsNumber()) return;

	RE::GFxValue selectionWidget;
	widgetMenu->uiMovie->GetVariable(&selectionWidget, "mc_SelectionWidget");

	if (!selectionWidget.IsObject()) return;
	auto activateText = GetGFxMember(selectionWidget, "ActivateText");

	RE::GFxValue lineMetricsOut;
	RE::GFxValue args[1]{ 1 };
	activateText.Invoke("getLineMetrics", &lineMetricsOut, args, 1);
	if (!lineMetricsOut.IsObject()) return;

	double sizeFactor = WidgetSize.x / 100.0;

	auto grayBar = GetGFxMember(selectionWidget, "GrayBar");
	if (!grayBar.IsObject())
		return;

	// aspect ratio fix
    RECT windowRect;
    GetClientRect(GetForegroundWindow(), &windowRect);

	RE::GRectF stageRect = widgetMenu->uiMovie->GetVisibleFrameRect();

    float originalAspectRatio = stageRect.bottom / stageRect.right;
    float aspectRatio = (float)windowRect.bottom / (float)windowRect.right;
    float aspectRatioMult = 1.0f - (aspectRatio / originalAspectRatio);
    //

	auto grayBarHalfWidth = GetGFxMember(grayBar, "_width").GetNumber();
	grayBarHalfWidth = PixelToStageCoordinates(widgetMenu, glm::vec2(grayBarHalfWidth, 0.0)).x * sizeFactor;

	auto lineHalfHeight = (GetGFxMember(lineMetricsOut, "height").GetNumber() / 2.0);
    lineHalfHeight = PixelToStageCoordinates(hud, glm::vec2(0.0, lineHalfHeight)).y * sizeFactor;
	auto lineHalfWidth = GetGFxMember(lineMetricsOut, "width").GetNumber();
	lineHalfWidth = PixelToStageCoordinates(hud, glm::vec2(lineHalfWidth, 0.0)).x * sizeFactor;

	double iconRealSize = iconSize.GetNumber() * iconScale.GetNumber() * sizeFactor;

	glm::vec2 offsetWidgetPos;
    if (Compatibility::SkyHUD::IsEnabled)
    {
        offsetWidgetPos = Compatibility::SkyHUD::CalculateWidgetOffset(hudRoot, hud, WidgetPos);

		// I don't even know anymore - changing the aspect ratio with SkyHUD resizes an already resized
		// HUD, which causes the text width results to be off. Where did I get this 1.25f? Trial
		// and error. No idea why this is the exact value the text width needs to be scaled with
        lineHalfWidth *= 1.25f - (2.0f * aspectRatioMult);
        lineHalfHeight *= 1.25f - (2.0f * aspectRatioMult);
    }
    else
    {
        offsetWidgetPos = glm::vec2(
            -GetGFxMember(hudRoot, "_x").GetNumber() + WidgetPos.x,
            -GetGFxMember(hudRoot, "_y").GetNumber() + WidgetPos.y);
    }

	// position icons
	int arraySize = loadedIcons.GetArraySize();

	for (int i = 0; i < arraySize; i++)
	{
		RE::GFxValue currIcon;
		loadedIcons.GetElement(i, &currIcon);

		if (!currIcon.IsObject())
			continue;

		// align with right side of selection widget textfield
        double newX = offsetWidgetPos.x + (i * MoreHUDIconSpace * sizeFactor) + (i * iconRealSize * sizeFactor) + lineHalfWidth;
        double newY = offsetWidgetPos.y - lineHalfHeight;

		currIcon.SetMember("_xscale", iconRealSize);
        currIcon.SetMember("_yscale", iconRealSize);

		currIcon.SetMember("_x", newX);
		currIcon.SetMember("_y", newY);
	}

	// position 'content', which is the MoreHUD widget that displays
	// magic effects on alchemical substances and food for example
	RE::GFxValue widgetCont;
	hud->uiMovie->GetVariable(&widgetCont, "AHZWidgetContainer");
	if (!widgetCont.IsObject()) return;

	auto ahzWidget = GetGFxMember(widgetCont, "AHZWidget");
	if (!ahzWidget.IsObject()) return;

	auto content = GetGFxMember(ahzWidget, "content");
	if (!content.IsObject()) return;

	double effectsSize = WidgetSize.x * MoreHUDEffectsSizeFact;
	content.SetMember("_xscale", effectsSize);
	content.SetMember("_yscale", effectsSize);

	auto contentHalfHeight = GetGFxMember(content, "_height").GetNumber() / 2.0;
	double infoWidgetCenterOffset = std::max(grayBarHalfWidth, lineHalfWidth) + InfoWidgetOffsetLeft;

	glm::vec2 containerPos(WidgetPos.x + infoWidgetCenterOffset, WidgetPos.y - contentHalfHeight);
    containerPos.x += (aspectRatioMult * stageRect.right);

    content.SetMember("_x", containerPos.x);
    content.SetMember("_y", containerPos.y);
}

void SelectionWidget::SetProgressCirlceValue(float newValue)
{
	if (Settings::IsDismountProgressCirlcleEnabled)
		ProgressCircleValue = newValue;
	else
		ProgressCircleValue = 0.0;
}

double SelectionWidget::GetWidgetTargetSize(FocusObject* focusObj)
{
    if (!focusObj || !focusObj->ObjectRef.get())
		return Settings::GetWidgetSizeMin();

	glm::dvec3 widgetPos;
	if (!Get3DWidgetPos(focusObj, widgetPos))
		return Widget2DSize;

	glm::vec3 glmWidgetPos(widgetPos.x, widgetPos.y, widgetPos.z);

	glm::vec3 cameraPos = Util::GetCameraPos();
	double distToObject = glm::length(cameraPos - glmWidgetPos);

	double distFactor = std::min(distToObject / GetMaxCameraDist(), 1.0);
	double distFactorInv = 1 - distFactor;

	double widgetSizeMin = Settings::GetWidgetSizeMin();
	double widgetSizeMax = Settings::GetWidgetSizeMax();

	return distFactorInv * (widgetSizeMax - widgetSizeMin) + widgetSizeMin;
}

glm::vec2 SelectionWidget::GetScreenLoc(RE::GPtr<RE::IMenu> menu, glm::vec3 worldPos)
{
	glm::vec3 screenLoc;
	RE::NiCamera::WorldPtToScreenPt3((float(*)[4])Offsets::WorldToCamMatrix, *Offsets::ViewPort,
		RE::NiPoint3((float)worldPos.x, (float)worldPos.y, (float)worldPos.z),
		screenLoc.x, screenLoc.y, screenLoc.z, 1e-5f);

	return GetScreenLocFromPercentage(menu, screenLoc);
}

glm::vec2 SelectionWidget::GetScreenLocFromPercentage(RE::GPtr<RE::IMenu> menu, glm::vec2 posIn)
{
    if (!menu.get())
        return posIn;

	glm::vec2 posOut = posIn;
    RE::GRectF rect = SelectionWidgetMenu::GetMenuRect(menu.get());

	posOut.x = rect.left + (rect.right - rect.left) * posOut.x;
	posOut.y = 1.0f - posOut.y;  // Flip y for Flash coordinate system
	posOut.y = rect.top + (rect.bottom - rect.top) * posOut.y;

	return posOut;
}

glm::vec2 SelectionWidget::ClampWidgetToScreenRect(RE::GPtr<RE::IMenu> menu, glm::vec2 posIn, RE::GFxValue selectionWidget)
{
	if (!selectionWidget.IsObject() || !menu.get())
		return posIn;

	auto activateText = GetGFxMember(selectionWidget, "ActivateText");
	if (!activateText.IsObject())
		return posIn;

	RE::GRectF rect = SelectionWidgetMenu::GetMenuRect(menu.get());

	auto gfxTextWidth = GetGFxMember(activateText, "textWidth").GetNumber();
	auto gfxWidgetHeight = GetGFxMember(selectionWidget, "_height").GetNumber();
	auto textBounds = PixelToStageCoordinates(menu, glm::vec2(gfxTextWidth, gfxWidgetHeight));

	if (posIn.x < textBounds.x)
		posIn.x = textBounds.x;
	else if (posIn.x > rect.right - textBounds.x)
		posIn.x = rect.right - textBounds.x;

	if (posIn.y < textBounds.y)
		posIn.y = textBounds.y;
	else if (posIn.y > rect.bottom - textBounds.y)
		posIn.y = rect.bottom - textBounds.y;

	return posIn;
}

bool SelectionWidget::Get3DWidgetPos(FocusObject* focusObj, glm::dvec3& posOut)
{
    if (!focusObj || !focusObj->ObjectRef.get())
		return false;

	auto focusRef = focusObj->ObjectRef.get().get();

	// place widget over characters' head, but in center for other objects, or for dead characters
	auto characterObject = focusRef->As<RE::Actor>();

	ObjectOverride objOverride;
	bool hasOverride = Settings::GetObjectOverride(focusRef, objOverride);

	if (characterObject && (
		characterObject->actorState1.lifeState == RE::ACTOR_LIFE_STATE::kAlive ||
		characterObject->actorState1.lifeState == RE::ACTOR_LIFE_STATE::kBleedout ||
		characterObject->actorState1.lifeState == RE::ACTOR_LIFE_STATE::kDying ||
		characterObject->actorState1.lifeState == RE::ACTOR_LIFE_STATE::kEssentialDown ||
		characterObject->actorState1.lifeState == RE::ACTOR_LIFE_STATE::kRestrained ||
		characterObject->actorState1.lifeState == RE::ACTOR_LIFE_STATE::kReanimate
		))
	{
		auto characterHead = Util::GetCharacterHead(focusRef);
		if (characterHead)
		{
			auto worldTranslate = characterHead->world.translate;
			posOut = glm::dvec3(worldTranslate.x, worldTranslate.y, worldTranslate.z);
		}
		else
			posOut = Util::GetBoundingBoxCenter(focusObj);

		posOut.z += Settings::GetCurrentWidgetZOffset();
	}
	else
	{
		if (hasOverride)
        {
            Get3DWidgetBasePos(focusObj, posOut, objOverride.WidgetBasePos);
			
			if (objOverride.HasWidgetPosOffset)
            {
                auto rotation = focusObj->CollisionObject.BoundingBox.rotation;
                posOut += objOverride.WidgetPosOffset;
            }
			else if (objOverride.HasWidgetPosOffsetRel)
			{
                auto rotation = focusObj->CollisionObject.BoundingBox.rotation;
                posOut += Util::RotateVector(rotation, objOverride.WidgetPosOffset);
			}
        }
		else
		{
            Get3DWidgetBasePos(focusObj, posOut, ObjectOverride::WidgetPos::Auto);
		}
	}
	
	return true;
}

bool SelectionWidget::Get3DWidgetBasePos(FocusObject* focusObj, glm::dvec3& posOut, ObjectOverride::WidgetPos pos)
{
	switch (pos)
	{
    case ObjectOverride::WidgetPos::Auto:

		return Get3DWidgetBasePosAuto(focusObj, posOut);

    case ObjectOverride::WidgetPos::Center:

		posOut = Util::GetBoundingBoxCenter(focusObj);
        return true;

    case ObjectOverride::WidgetPos::Top:

		posOut = Util::GetBoundingBoxTop(focusObj);
        return true;

    case ObjectOverride::WidgetPos::Bottom:

		posOut = Util::GetBoundingBoxBottom(focusObj);
        return true;

    case ObjectOverride::WidgetPos::Root:

		posOut = Util::GetObjectAccuratePosition(focusObj->ObjectRef.get().get());
        return true;
	}

	return false;
}

bool SelectionWidget::Get3DWidgetBasePosAuto(FocusObject* focusObj, glm::dvec3& posOut)
{
    auto focusRef = focusObj->ObjectRef.get().get();

    auto mesh = focusRef->GetCurrent3D();
    if (!mesh)
        return false;

    auto collisionSize = FocusObject::GetCollisionSize(focusObj->CollisionObject.BoundingBox.boundMax.z);

    switch (collisionSize)
    {
    case Tiny:
        posOut = Util::GetBoundingBoxTop(focusObj);

		posOut.z += Settings::GetCurrentWidgetZOffset();
        break;
    case Small:
        posOut = Util::GetBoundingBoxTop(focusObj);

		posOut.z += Settings::GetCurrentWidgetZOffset();
        break;
    default:
        posOut = Util::GetBoundingBoxCenter(focusObj);
        break;
    }

	return true;
}

glm::dvec2 SelectionWidget::Get3DWidgetSize(FocusObject* focusObj)
{
	double scaleFactor = GetWidgetTargetSize(focusObj);
	return glm::vec2(glm::vec2(OrigMCSize.x * scaleFactor, OrigMCSize.y * scaleFactor));
}

glm::dvec2 SelectionWidget::Get2DWidgetPos(RE::GPtr<RE::IMenu> menu)
{
	return GetScreenLocFromPercentage(menu, Widget2DPos);
}

glm::dvec2 SelectionWidget::Get2DWidgetSize()
{
	return glm::vec2(OrigMCSize.x * Widget2DSize, OrigMCSize.y * Widget2DSize);
}

glm::vec2 SelectionWidget::PixelToStageCoordinates(RE::GPtr<RE::IMenu> menu, glm::vec2 vecIn)
{
    RECT windowRect;
    GetClientRect(GetForegroundWindow(), &windowRect);

	RE::GRectF stageRect = menu->uiMovie->GetVisibleFrameRect();

	vecIn.x *= (stageRect.right / windowRect.right);
    vecIn.y *= (stageRect.bottom / windowRect.bottom);

	return vecIn;
}

glm::vec2 SelectionWidget::StageToPixelCoordinates(RE::GPtr<RE::IMenu> menu, glm::vec2 vecIn)
{
	if (!menu || !menu->uiMovie)
		return vecIn;

	RECT windowRect;
    GetClientRect(GetForegroundWindow(), &windowRect);

	RE::GRectF stageRect = menu->uiMovie->GetVisibleFrameRect();

	vecIn.x *= windowRect.right / stageRect.right;
    vecIn.y *= windowRect.bottom / stageRect.bottom;

	return vecIn;
}

void SelectionWidget::SetElementPos(glm::vec2 screenPos, RE::GFxValue& element)
{
	element.SetMember("_x", screenPos.x);
	element.SetMember("_y", screenPos.y);
}

void SelectionWidget::SetElementSize(glm::vec2 size, RE::GFxValue& element)
{
 	element.SetMember("_xscale", size.x);
 	element.SetMember("_yscale", size.y);
}

void SelectionWidget::InitHUDValues()
{
	if (OriginalValuesSet)
		return;

	auto ui = RE::UI::GetSingleton();
	if (!ui)
		return;

	auto hud = ui->GetMenu(RE::HUDMenu::MENU_NAME);
	if (!hud || !hud->uiMovie)
		return;

	auto widgetMenu = ui->GetMenu(SelectionWidgetMenu::MENU_NAME);
	if (!widgetMenu || !widgetMenu->uiMovie)
		return;

	RE::GFxValue selectionWidget;
	widgetMenu->uiMovie->GetVariable(&selectionWidget, "mc_SelectionWidget");

	if (!selectionWidget.IsObject())
		return;

	OrigMCPos = glm::vec2(
		GetGFxMember(selectionWidget, "_x").GetNumber(),
		GetGFxMember(selectionWidget, "_x").GetNumber());

	OrigMCSize = glm::vec2(
		GetGFxMember(selectionWidget, "_xscale").GetNumber(),
		GetGFxMember(selectionWidget, "_yscale").GetNumber());

	OriginalValuesSet = true;

	Compatibility::MoreHUD::CheckModuleLoaded();
	Compatibility::SkyHUD::CheckModuleLoaded();

	logger::info("BTPS: successfully finished InitHUDValues");
}

void SelectionWidget::OnControlsToggled(bool mode)
{
	if (mode)
		SelectionWidgetMenu::Show("ControlsToggled");
	else
		SelectionWidgetMenu::Hide("ControlsToggled");
}

void SelectionWidget::OnFreeCamToggled(bool mode)
{
	if (!mode)
		SelectionWidgetMenu::Show("FreeCamToggled");
	else
		SelectionWidgetMenu::Hide("FreeCamToggled");
}

SelectionWidgetMenu::SelectionWidgetMenu()
{
	auto scaleformManager = RE::BSScaleformManager::GetSingleton();
	if (!scaleformManager)
	{
		logger::error("BTPS: failed to initialize SelectionWidgetMenu");
		return;
	}

	depthPriority = 0;

	menuFlags.set(RE::UI_MENU_FLAGS::kAlwaysOpen);
	menuFlags.set(RE::UI_MENU_FLAGS::kRequiresUpdate);
	menuFlags.set(RE::UI_MENU_FLAGS::kAllowSaving);

	inputContext = Context::kNone;

	if (uiMovie)
	{
		uiMovie->SetMouseCursorCount(0); // disable input
		uiMovie->SetVisible(false);
	}

	scaleformManager->LoadMovieEx(this, MENU_PATH, [this](RE::GFxMovieDef* a_def) -> void
	{
		a_def->SetState(RE::GFxState::StateType::kLog,
			RE::make_gptr<Logger>().get());

		logger::info("BTPS: SelectionWidgetMenu loaded flash");
	});

	InitProgressCirclePosition(this);
}

void SelectionWidgetMenu::InitProgressCirclePosition(SelectionWidgetMenu* menu)
{
	if (!menu || !menu->uiMovie)
		return;

	RE::GFxValue progressCirlce;
	menu->uiMovie->GetVariable(&progressCirlce, "ProgressCircleInstance");

	if (!progressCirlce.IsObject())
		return;

	auto menuRect = GetMenuRect(menu);

	progressCirlce.SetMember("_x", menuRect.right / 2.0f);
    progressCirlce.SetMember("_y", menuRect.bottom / 2.0f);
}

void SelectionWidgetMenu::Register()
{
	auto ui = RE::UI::GetSingleton();
	if (ui)
	{
		ui->Register(MENU_NAME, Creator);

		logger::info("BTPS: successfully registered SelectionWidgetMenu");
	}
	else
		logger::error("BTPS: failed to register SelectionWidgetMenu");
}

RE::GRectF SelectionWidgetMenu::GetMenuRect(RE::IMenu* menu)
{
    if (!menu || !menu->uiMovie)
        return RE::GRectF();

    return menu->uiMovie->GetVisibleFrameRect();
}

void SelectionWidgetMenu::Load()
{
	auto msgQ = RE::UIMessageQueue::GetSingleton();
	if (msgQ)
	{
		msgQ->AddMessage(MENU_NAME, RE::UI_MESSAGE_TYPE::kShow, nullptr);
	}
	else
		logger::warn("BTPS: failed to load SelectionWidgetMenu");
}

void SelectionWidgetMenu::Unload()
{
	auto msgQ = RE::UIMessageQueue::GetSingleton();
	if (msgQ)
	{
		msgQ->AddMessage(MENU_NAME, RE::UI_MESSAGE_TYPE::kHide, nullptr);
	}
	else
		logger::warn("BTPS: failed to unload SelectionWidgetMenu");
}

void SelectionWidgetMenu::Show(std::string source)
{
	if (!source.empty())
	{
		auto sourceIdx = std::find(Hidden_Sources.begin(), Hidden_Sources.end(), source);
		if (sourceIdx != Hidden_Sources.end())
			Hidden_Sources.erase(sourceIdx);
	}

	if (IsEnabled && Hidden_Sources.empty())
		ToggleVisibility(true);
}

void SelectionWidgetMenu::Hide(std::string source)
{
	auto sourceIdx = std::find(Hidden_Sources.begin(), Hidden_Sources.end(), source);
	if (sourceIdx == Hidden_Sources.end())
		Hidden_Sources.push_back(source);

	if (!Hidden_Sources.empty())
		ToggleVisibility(false);
}

void SelectionWidgetMenu::ToggleVisibility(bool mode)
{
	auto ui = RE::UI::GetSingleton();
	if (!ui)
		return;

	auto widgetMenu = ui->GetMenu(SelectionWidgetMenu::MENU_NAME);
	if (!widgetMenu || !widgetMenu->uiMovie)
		return;

	widgetMenu->uiMovie->SetVisible(mode);

	//SelectionWidget::HideNativeHUD();

	if (Compatibility::MoreHUD::IsEnabled)
	{
		auto hud = ui->GetMenu(RE::HUDMenu::MENU_NAME);
		if (!hud || !hud->uiMovie) return;

		RE::GFxValue iconCont;
		hud->uiMovie->GetVariable(&iconCont, "ahz.scripts.widgets.AHZHudInfoWidget.IconContainer");
		if (!iconCont.IsObject()) return;

		if (mode)
			iconCont.Invoke("Show");
		else
			iconCont.Invoke("Hide");

		RE::GFxValue widgetCont;
		hud->uiMovie->GetVariable(&widgetCont, "AHZWidgetContainer.AHZWidget.content");
		if (!widgetCont.IsObject()) return;

		widgetCont.SetMember("_visible", mode);
	}

	IsVisible = mode;
}

void SelectionWidgetMenu::AdvanceMovie(float a_interval, std::uint32_t a_currentTime)
{
	RE::IMenu::AdvanceMovie(a_interval, a_currentTime);
	
	SelectionWidget::Update();
}
