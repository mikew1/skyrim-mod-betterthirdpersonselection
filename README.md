# Skyrim Mod - BetterThirdPersonSelection

Skyrim SKSE (CommonLib) plugin to overhaul third person object selection to align more with exclusively third person games

## For Users

If you want to find out about how to configure Better Third Person Selection, check out the [Wiki](https://gitlab.com/Shrimperator/skyrim-mod-betterthirdpersonselection/-/wikis/home)

## For Mod Authors

### API
If you're a plugin author and want to use the Better Third Person Selection mod API, take a look at the [Wiki page](https://gitlab.com/Shrimperator/skyrim-mod-betterthirdpersonselection/-/wikis/Plugin%20API)

### Getting started

- install [CMake](https://cmake.org/) - during installation, enable adding to path
- install [vcpkg](https://vcpkg.io/en/index.html)
- install **Visual Studio 2022** with *'Desktop development with C++'* enabled. Make sure it installs the Windows 10 SDK and MSVC v143 build tools or later
- clone Better Third Person Selection master wherever, using **--recursive** to clone submodules as well (eg. from cmd 'git clone --recursive https://gitlab.com/Shrimperator/skyrim-mod-betterthirdpersonselection.git'), and navigate inside the cloned folder
- run **build_CMakeGenerate.bat** to let CMake generate the VS project files
- open *build/pre-ae/BetterThirdPersonSelection.sln* or *build/post-ae/BetterThirdPersonSelection.sln* in Visual Studio
- **compile**

Some environment variables you might need:
- **VCPKG_ROOT**: absolute path to wherever you have vcpkg installed
- **Skyrim64Path**: absolute path to the folder containing your SkyrimSE.exe

### Tips

- when publishing, compile using the config **RelWithDebInfo**
- depending on whether you want to compile for SE or AE, use the .sln either in the folder *pre-ae (SE)* or *post-ae (AE)*
- if you're lazy, use **Releases/RelWithDebInfo_StageAndPack.bat** to generate a zip file containing the mod binaries and assets in installable form for both SE and AE. Make sure you compile for whichever version you want packed beforehand. The zip files will be in *Releases/REL_SE* and *Releases/REL_AE*
